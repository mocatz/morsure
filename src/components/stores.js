import { writable } from 'svelte/store';

export const timerDot = writable(1000);
export const timerLine = writable(3000);
export const timerLetter = writable(1000);
export const timerWord = writable(7000);
export const morseAudioOn = writable(false);
