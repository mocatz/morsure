/**
 * FROM CSS ROOT:
 * color-0
 * color-1
 * color-2
 * color-3
 * color-4
 * 
 * FROM STORE:
 * timerDot
 * timerLine
 * timerLetter
 * timerWord
 * morseAudioOn
 */

import { timerDot, timerLine, timerLetter, timerWord, morseAudioOn } from "./stores";

const LOCAL_STORAGE_NAME = 'morsureSettings';

export function userSettings(settings) {
  const userSettings = settings;

  function saveUserSettings(k, v) {
    userSettings[k] = v;
    localStorage.setItem(LOCAL_STORAGE_NAME, JSON.stringify(userSettings));
  }
  return saveUserSettings;
}

export function loadUserSettings() {
  const userSettings = JSON.parse(localStorage.getItem(LOCAL_STORAGE_NAME));
  return userSettings || {};
}

export function setUserSettings(settings) {
  for (const [k, v] of Object.entries(settings)) {
    if(k.startsWith('--')) {
      document.documentElement.style.setProperty(k, v);
    } else {
      switch (k) {
        case 'timerDot':
          timerDot.update(() => v);
          break;
        case 'timerLine':
          timerLine.update(() => v);
          break;
        case 'timerLetter':
          timerLetter.update(() => v);
          break;
        case 'timerWord':
          timerWord.update(() => v);
        case 'morseAudioOn':
          morseAudioOn.update(() => v);
          break;
      }
    }
  }
}

export function deleteLocalStorageSettings() {
  localStorage.removeItem(LOCAL_STORAGE_NAME);
}
